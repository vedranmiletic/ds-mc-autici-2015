# Monte Carlo simulacija punjenja autića na baterije

Završni projekt iz kolegija [Distribuirani sustavi] na [Odjelu za informatiku Sveučilišta u Rijeci].

#### Akademska godina 2014./2015.

### Autori
 * Jovan Jokić, *univ. bacc. inf.*
 * Kristijan Lenković, *univ. bacc. inf.*
 * Ivana Pilaj, *univ. bacc. inf.*


### Requirements
 * Python >= 3.2.3
 * Open MPI >= 1.4.5
 * mpi4py >= 1.3+hg20120611-3

### Development

Glavni dev repozitorij dostupan je preko BitBucket servisa.
```sh
$ git clone https://[username]@bitbucket.org/infuniri/ds-mc-autici-2015.git
```

### Literatura

 * [NETWORK MODELING AND SIMULATION] 69. str - 96. str


[Distribuirani sustavi]:http://inf2.uniri.hr/reStructuredHgWiki/kolegiji/DS.html
[NETWORK MODELING AND SIMULATION]:http://podelise.ru/tw_files/23602/d-23601392/7z-docs/1.pdf
[Odjelu za informatiku Sveučilišta u Rijeci]:http://www.inf.uniri.hr/

