import numpy as np
from sys import argv
from mpi4py import MPI
import time

def printHelp():
    print("\nParametri redom: [vrijeme izvodjenja] [interval] [brzina punjenja (izmedju 0 i 1)]")
    print("Primjer pozivanja simulacije:")
    print("$ mpirun -np 4 python3 autici-mpi.py 60 2 0.5")

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

try:
    assert size > 1, "GRESKA: Broj procesa mora biti barem 2!"
    vrijemeIzvodjenja = int(argv[1])
    interval = int(argv[2])
    brzinaPunjenja = float(argv[3])
    assert brzinaPunjenja >= 0 and brzinaPunjenja <= 1, "GRESKA: Brzina punjenja mora biti vrijednost izmedju 0 i 1"
except AssertionError as e:
    if rank==0:
        print(e)
        printHelp()
    exit()
except IndexError:
    if rank==0:
        print("GRESKA: Krivi format parametara!")
        printHelp()
    exit()

brojSimulacija = size
brAutica = int(vrijemeIzvodjenja/interval)

if rank==0:
    print("Vrijeme izvodjenja:", vrijemeIzvodjenja)
    print("Interval dolazaka autica:", interval)
    print("Brzina punjenja baterije autica:", brzinaPunjenja)
    print("Broj simulacija:", brojSimulacija)
    print("Broj autica:", brAutica)

if rank==0:
    auticiSvi = np.random.rand(brojSimulacija, brAutica)
    #print("svi:",auticiSvi)
    maxQ = np.zeros(brojSimulacija, dtype=np.int32)
    kor = np.zeros(brojSimulacija, dtype=np.float32)
else:
    auticiSvi = None
    maxQ = None
    kor = None

auti = np.zeros(brAutica)

#comm.Barrier()

#print("Broj trenutne simulacije:", rank)
comm.Scatter(auticiSvi, auti, root=0)

'''
Simulacija
'''

#print("auti:",auti)

trenutniAutic = auti[0]
auti[0] = -1
brPumpaNeRadi = 0
maxQSim = 0
red = np.empty(brAutica, dtype=np.int32)
ipp = 0 # index prvog praznog iz reda
ippu = 1 # index prvog punog iz vanjsog reda

for i in range(0, brAutica):
    red[i] = -1

for i in range(0, vrijemeIzvodjenja+1):
    if i % interval == 0 and i != 0:
        #print("i-usao")
        vanjskiRedPrazan = False
        for j in range(ippu, brAutica):
            #print("i",i,"j",j,"ipp",ipp,"ippu",ippu,"auti[j]",auti[j])
            vanjskiRedPrazan = True if auti[j] == -1 else False
            if not vanjskiRedPrazan:
                break
        #print("usao1", vanjskiRedPrazan)
        if not vanjskiRedPrazan:
            #print("usao2")
            while ipp < brAutica-1:
                ipp = ipp + 1
                if red[ipp-1] == -1:
                    break
            while ippu < brAutica-1:
                ippu = ippu + 1
                if auti[ippu-1] != -1:
                    break
            #print("i",i,"ipp",ipp,"ippu",ippu,"red",red,"auti",auti)
            red[ipp] = auti[ippu]
            auti[ippu] = -1
            brQ = 0
            for k in range(0, brAutica):
                #print("red",red)
                if red[k] != -1:
                    brQ = brQ + 1
            #print("brq:",brQ)
            maxQSim = maxQSim if maxQSim > brQ else brQ

    if trenutniAutic < 1:
        trenutniAutic = trenutniAutic + brzinaPunjenja
    else:
        redPrazan = True
        for j in range(ipp, brAutica):
            redPrazan = True if red[j] == -1 else False
            if not redPrazan:
                break
        if not redPrazan:
            for j in range(ipp, brAutica):
                trenutniAutic = red[j] if red[j] == -1 else trenutniAutic
                if red[j] != -1:
                    red[j] = -1
                    ipp = ipp + 1
                    break
            trenutniAutic = trenutniAutic + brzinaPunjenja
        else:
            brPumpaNeRadi = brPumpaNeRadi + 1

maxQdoneval = np.int32(maxQSim)
kordoneval = np.float32((vrijemeIzvodjenja-brPumpaNeRadi)*100/vrijemeIzvodjenja)

maxQdone = np.array([maxQdoneval], dtype=np.int32)
kordone = np.array([kordoneval], dtype=np.float32)


'''
kraj simulacije
'''

comm.Gather(maxQdone, maxQ, root=0)
comm.Gather(kordone, kor, root=0)

if rank == 0:
    print("Simulacija zavrsila")
    print("====================================================================")
    print("Rezultati simulacije")
    print("Maksimalni broj autiju u redu cekanja:", maxQ)
    print("Korisnosti pumpe:", kor)
    print("====================================================================")
    print("Prosjecan red cekanja:", maxQ.mean())
    print("Prosjecna korisnost:", kor.mean(), "%")
